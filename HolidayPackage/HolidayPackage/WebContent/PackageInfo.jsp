<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="InsertDetails.jsp" method="post">

<table bgcolor="pink" align="center">

<tr>
<td colspan=2>
<center><font size=4><b>Package Details</b></font></center>
</td>
</tr>

<tr>
<td>Destination Place</td>
<td><input type="text" name="destination_place" required pattern="[a-zA-Z ]+"></td>
</tr>

<tr>
<td>Tour cost per person:</td>
<td><input type="number" name="tcost" size="30" required></td>
</tr>

<tr>
<td>Tour days:</td>
<td><input type="number" name="tdays" size="30" required min="1"></td>
</tr>

<tr>
<td>Package inclusions</td>
<td><textarea rows="3" cols="20" name="pinclude" required></textarea></td>
</tr>

<tr>
<td>No of days:</td>
<td><input type="number" name="nodays" size="30" required min="1"></td>
</tr>

<tr>
<td>No of nights:</td>
<td><input type="number" name="nonights" size="30" required min="0"></td>
</tr>

<tr>
<td>Air flight cost include:</td>
<td>
<input type="radio" name="cinclude" value="y" size="10">Yes
<input type="radio" name="cinclude" value="n" size="10">No
</td>
</tr>

<tr>
<td><input type="Submit" value="Submit">
</tr>

</table>
</form>
</body>
</html>
