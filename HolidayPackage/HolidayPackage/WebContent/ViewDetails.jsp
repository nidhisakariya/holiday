<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<div class="topnav">
  <a  href="AdminHome.jsp">Home</a>
  <a href="PackageInfo.jsp">Insert Package</a>
  <a class="active" href="ViewDetails.jsp">Manage Packages</a>
</div>
<form action="" method="post">
<%
	try
	{
		Class.forName("com.mysql.jdbc.Driver");  
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/project","root","root");  
		
		String s="select * from Tbl_D_Holidays";
		PreparedStatement pst1=con.prepareStatement(s); 
		
		ResultSet rs=pst1.executeQuery();
		
		%>
		<table border="2">
		<tr>
		<td>Destination Place</td>
		<td>Tour cost per person</td>
		<td>Tour days</td>
		<td>Package inclusions</td>
		<td>No of days</td>
		<td>No of nights</td>
		<td>Air flight cost include</td>
		<td>Package Availability</td>
		</tr>
		<% 
		
		while(rs.next())
		{
			%>
			<tr>
			    <td><%=rs.getString(2)%></td>
			    <td><%=rs.getInt(3) %></td>
			    <td><%=rs.getInt(4)%></td>
			    <td><%=rs.getString(5)%></td>
			    <td><%=rs.getInt(6)%></td>
			    <td><%=rs.getInt(7)%></td>
			    <td><%=rs.getString(8)%></td>
			    <td><%=rs.getString(9)%></td>
			    <% 
			    if(rs.getString(9).equals("d"))
			    		{
			    %>
			    <td><a href="Active.jsp?id=<%=rs.getInt(1)%>">Active</a></td>
		  
		  <%
			    		}
		  else
		  {
			  %>
			  	 <td><a href="Deactive.jsp?id=<%=rs.getInt(1)%>">Deactive</a></td>
			  <% 
			  
		  }
		}
		
	 %>
		  </tr>	
		  </table>
<%
		}
	catch(Exception e)
	{
		out.println("Error!"+e);
	}
%>
</form>
</body>
</html>