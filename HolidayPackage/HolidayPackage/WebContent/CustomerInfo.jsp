 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<div class="topnav">
  <a href="#home">Home</a>
  <a class="active" href="CustomerInfo.jsp">Search Packages</a>
 </div>
<form action="DisplayPackages.jsp" method="post">

<table bgcolor="pink" align="center">

<tr>
<td colspan=2>
<center><font size=4><b>Package Details</b></font></center>
</td>
</tr>

<tr>
<td>Name:</td>
<td><input type="text" name="name" size="30" required pattern="[a-zA-Z ]+"></td>
</tr>

<tr>
<td>No of person:</td>
<td><input type="number" name="person" size="30" required min="1"></td>
</tr>

<tr>
<td>Budget:</td>
<td><input type="number" name="budget" size="30" required min="5000"></td>
</tr>

<tr>
<td><input type="Submit" value="Submit">
</tr>

</table>
</form>
</body>
</html>