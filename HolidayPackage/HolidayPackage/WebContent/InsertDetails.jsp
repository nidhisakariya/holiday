<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<div class="topnav">
  <a  href="AdminHome.jsp">Home</a>
  <a class="active" href="PackageInfo.jsp">Insert Package</a>
  <a href="ViewDetails.jsp">Manage Packages</a>
</div>
<%
	String dp=request.getParameter("destination_place");
	int cost=Integer.parseInt(request.getParameter("tcost"));
	int days=Integer.parseInt(request.getParameter("tdays"));
	String pinfo=request.getParameter("pinclude");
	int nd=Integer.parseInt(request.getParameter("nodays"));
	int nn=Integer.parseInt(request.getParameter("nonights"));
	String a=request.getParameter("cinclude");
%>
<%
	try
	{
		Class.forName("com.mysql.jdbc.Driver");  
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/project","root","root");  
		
		String s="select * from Tbl_D_Holidays where Destination_Place=?";
		PreparedStatement pst1=con.prepareStatement(s); 
		pst1.setString(1,dp);
		
		ResultSet rs=pst1.executeQuery();
		if(rs.next() && rs.getInt(3)==cost && rs.getInt(4)==days)
		{
				out.println("This package already exists");
		}
		else
		{
			String q="insert into Tbl_D_Holidays(Destination_Place,Tour_Cost_Per_Person,Tour_Days,Package_Inclusions,Number_of_Days,Number_of_Night,Air_Flight_Cost_Include,Status) values(?,?,?,?,?,?,?,?)";
			
			PreparedStatement pst=con.prepareStatement(q); 
			pst.setString(1,dp);
			pst.setInt(2,cost);
			pst.setInt(3,days);
			pst.setString(4,pinfo);
			pst.setInt(5,nd);
			pst.setInt(6,nn);
			pst.setString(7,a);
			pst.setString(8,"a");
			
			int i=pst.executeUpdate();
			
			if(i==1)
			{
				out.println("Inserted");
			}
			else
			{
				out.println("Not inserted!");
			}
		}
			  
	}
	catch(Exception e)
	{
		out.println("Error!"+e);
	}
%>
</body>
</html>