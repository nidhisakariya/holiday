package com.Java;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Login extends SimpleTagSupport{
	private String uname;
	private String password;
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		// TODO Auto-generated method stub
		//super.doTag();
		JspWriter out=getJspContext().getOut();
		if(uname.equals("Admin") && password.equals("admin"))
		{
			out.println("<script>window.location.href='AdminHome.jsp'</script>");
		}
		else if(uname.equals("Customer") && password.equals("customer"))
		{
			out.println("<script>window.location.href='CustomerInfo.jsp'</script>");
		}
		else
		{
			out.println("Invalid user");
		}
	}
}
